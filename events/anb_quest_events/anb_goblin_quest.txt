﻿namespace = anb_goblin_quest

#Pick how to handle the quest
anb_goblin_quest.0001 = {
	title = anb_goblin_quest.0001.title
	desc = anb_goblin_quest.0001.description
	theme = court
	
	left_portrait = { character = scope:actor animation = worry }

	immediate = {
		scope:target = {
			set_variable = {
				name = searching_for_adventurers
			}
			
			save_scope_as = quest_target
		}
	}

	# Find an adventurer
	option = {
		name = anb_goblin_quest.0001.a
		search_for_adventurers_for_quest_effect = {
			QUEST = goblin_quest
		}
	}
	
	# Go question yourself
	option = {
		name = anb_goblin_quest.0001.b
		
		show_as_unavailable = {
			is_physically_able = yes
			is_available_adult_or_is_commanding = yes
		}
		
		character_go_on_quest_effect = {
			ADVENTURER = root
			LOCATION = scope:quest_target
			QUEST = goblin_quest
		}
	}
	
	# Go back
	option = {
		name = anb_goblin_quest.0001.c
		
		scope:quest_target = {
			remove_variable = searching_for_adventurers
		}
	}
}

#Pick an adventurer and send them out
anb_goblin_quest.0002 = {
	title = anb_goblin_quest.0002.title
	desc = anb_goblin_quest.0002.description
	theme = court
	
	left_portrait = scope:adventurer_to_hire_1
	right_portrait = scope:adventurer_to_hire_2
	
	immediate = {
		create_character = {
			save_scope_as = adventurer_to_hire_1
			template = anb_average_adventurer_character
			dynasty = none
			location = scope:quest_target 
		}
		create_character = {
			save_scope_as = adventurer_to_hire_2
			template = anb_average_adventurer_character
			dynasty = none
			location = scope:quest_target
		}
	}
	
	# Hire adventurer 1
	option = {
		name = anb_quest_hire.a
		
		send_hired_adventurer_for_quest_effect_with_default_stress_impact = {
			ADVENTURER = scope:adventurer_to_hire_1
			LOCATION = scope:quest_target
			QUEST = goblin_quest
		}
	}
	
	# Hire adventurer 2
	option = {
		name = anb_quest_hire.b
		
		send_hired_adventurer_for_quest_effect_with_default_stress_impact = {
			ADVENTURER = scope:adventurer_to_hire_2
			LOCATION = scope:quest_target
			QUEST = goblin_quest
		}
	}
	
	# Go back
	option = {
		name = anb_goblin_quest.0002.c
		
		scope:target = {
			remove_variable = searching_for_adventurers
		}
	}
}

# Track down the location of the goblins
anb_goblin_quest.0003 = {
	title = anb_goblin_quest.0003.title
	desc = anb_goblin_quest.0003.description
	theme = hunting
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			factor = 2
			scope:inspiration = {
				inspiration_progress >= 4
			}
		}
		modifier = {
			add = 1
			scope:inspiration = {
				inspiration_progress >= 3
			}
		}
		modifier = {
			add = 1
			scope:inspiration_owner = {
				has_hunter_trait = yes
			}
		}
		modifier = {
			add = -1
			scope:inspiration_owner = {
				has_trait = lazy
			}
		}
	}
	
	trigger = {
		scope:inspiration = {
			inspiration_progress < 5
			NOT = { has_variable = found_goblin_lair }
		}
	}
	
	# Use Martial
	option = {
		name = anb_goblin_quest.0003.a
		trigger = {
			NOT = {
				scope:inspiration_owner = {
					has_hunter_trait = yes
				}
			}
		}
		
		quest_duel_progress_1_effect = {
			TITLE = "anb_goblin_quest.0003.a.t"
			SUCCESS = "anb_goblin_quest.0003.a.duel.success"
			SUCCESS_EFFECT = "scope:inspiration = { set_variable = found_goblin_lair }"
			FAILURE = "anb_goblin_quest.0003.a.duel.failure"
			SKILL = "martial"
			DIFFICULTY = @very_easy_quest_duel
		}
	}
	
	# Use hunter skill
	option = {
		name = anb_goblin_quest.0003.b
		trigger = {
			scope:inspiration_owner = {
				has_hunter_trait = yes
			}
		}
		trait = hunter_1
		
		scope:inspiration = {
			change_inspiration_progress = 1
		}
	}
	
	# Use Diplomacy
	option = {
		name = anb_goblin_quest.0003.c
		
		quest_duel_progress_1_effect = {
			TITLE = "anb_goblin_quest.0003.c.t"
			SUCCESS = "anb_goblin_quest.0003.c.duel.success"
			SUCCESS_EFFECT = "scope:inspiration = { set_variable = found_goblin_lair }"
			FAILURE = "anb_goblin_quest.0003.c.duel.failure"
			SKILL = "diplomacy"
			DIFFICULTY = @very_easy_quest_duel
		}
	}
	
	# Hire locals
	option = {
		name = anb_goblin_quest.0003.d
		trigger = {
			AND = {
				gold >= minor_gold_value
				root = { is_ai = no }
			}
			short_term_gold >= minor_gold_value
		}
		
		remove_short_term_gold = minor_gold_value
		
		random_list = {
			85 = {
				scope:inspiration = {
					change_inspiration_progress = 1
				}
				
				scope:inspiration = { set_variable = found_goblin_lair }
				
				hidden_effect = {
					show_toast_for_owner_and_sponsor = {
						TITLE = "anb_goblin_quest.0003.d.t"
						TOOLTIP = "anb_goblin_quest.0003.d.success"
						ICON = "scope:inspiration_owner"
					}
				}
			}
			15 = {
				hidden_effect = {
					show_toast_for_owner_and_sponsor = {
						TITLE = "anb_goblin_quest.0003.d.t"
						TOOLTIP = "anb_goblin_quest.0003.d.failure"
						ICON = "scope:inspiration_owner"
					}
				}
			}
		}
	}
}

# Confrontation with some lame-ass goblins
anb_goblin_quest.0004 = {
	title = anb_goblin_quest.0004.title
	desc = anb_goblin_quest.0004.description
	theme = hunting
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			factor = 1.25
			scope:inspiration = {
				inspiration_progress >= 3
			}
		}
		# Should fight the boss instead
		modifier = {
			factor = 0.5
			scope:inspiration = {
				inspiration_progress >= 7
			}
		}
		
		modifier = {
			factor = 0.75
			scope:inspiration = {
				has_variable = defeated_goblin_band
			}
		}
	}
	
	# Use prowess
	option = {
		name = anb_goblin_quest.0004.a
	
		quest_duel_progress_1_effect = {
			TITLE = "anb_goblin_quest.0004.a.t"
			SUCCESS = "anb_goblin_quest.0004.a.duel.success"
			SUCCESS_EFFECT = "
				scope:inspiration = { set_variable = defeated_goblin_band }
				scope:inspiration_owner = { add_prestige = minor_prestige_gain }
			"
			FAILURE = "anb_goblin_quest.0004.a.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_owner = {
					add_prestige_no_experience = minor_prestige_loss
					increase_wounds_effect = { REASON = attacked }
				}
			"
			SKILL = "prowess"
			DIFFICULTY = @very_easy_quest_duel
		}
	}
	
	# Use intrigue, harder as goblins are sneaky buggers
	option = {
		name = anb_goblin_quest.0004.b
	
		quest_duel_progress_1_effect = {
			TITLE = "anb_goblin_quest.0004.b.t"
			SUCCESS = "anb_goblin_quest.0004.b.duel.success"
			SUCCESS_EFFECT = "
				scope:inspiration = { set_variable = defeated_goblin_band }
			"
			FAILURE = "anb_goblin_quest.0004.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_owner = {
					add_prestige_no_experience = minor_prestige_loss
					increase_wounds_effect = { REASON = attacked }
				}
			"
			SKILL = "intrigue"
			DIFFICULTY = @easy_quest_duel
		}
	}
	
	# Run away
	option = {
		name = anb_goblin_quest.0004.c
	
		scope:inspiration_owner = {
			add_prestige_no_experience = medium_prestige_loss
		}
	}
}

# Confrontation with the goblin boss
anb_goblin_quest.0005 = {
	title = anb_goblin_quest.0005.title
	desc = anb_goblin_quest.0005.description
	theme = hunting
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			factor = 2
			scope:inspiration = {
				inspiration_progress >= 7
			}
		}

		modifier = {
			factor = 1.25
			scope:inspiration = {
				has_variable = defeated_goblin_band
			}
		}
	}
	
	trigger = {
		scope:inspiration = {
			inspiration_progress <= 5
			has_variable = defeated_goblin_boss
		}
	}
	
	# Use prowess
	option = {
		name = anb_goblin_quest.0005.a
		
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0005.a.t"
			SUCCESS = "anb_goblin_quest.0005.a.duel.success"
			SUCCESS_EFFECT = "
				random_list = {
					# The goblins surrender
					33 = {
						scope:inspiration = {
							set_variable = defeated_goblin_boss
							change_inspiration_progress = 3
						}
						
						scope:inspiration_owner = {
							add_prestige = medium_prestige_gain
						}

						show_toast_for_owner_and_sponsor = {
							TITLE = anb_goblin_quest.0005.a.t
							ICON = scope:inspiration_owner
							TOOLTIP = anb_goblin_quest.0005.a.goblins_surrender
						}
					}
					# The goblins flee from their lair
					33 = {
						scope:inspiration = {
							set_variable = defeated_goblin_boss
							change_inspiration_progress = 2
						}
						
						scope:inspiration_owner = {
							add_prestige = medium_prestige_gain
						}
						
						show_toast_for_owner_and_sponsor = {
							TITLE = anb_goblin_quest.0005.a.t
							ICON = scope:inspiration_owner
							TOOLTIP = anb_goblin_quest.0005.a.goblins_flee
						}
					}
					# A new goblin boss arises
					33 = {
						scope:inspiration = {
							change_inspiration_progress = 1
						}
						
						scope:inspiration_owner = {
							add_prestige = medium_prestige_gain
						}
						
						show_toast_for_owner_and_sponsor = {
							TITLE = anb_goblin_quest.0005.a.t
							ICON = scope:inspiration_owner
							TOOLTIP = anb_goblin_quest.0005.a.new_goblin_boss
						}
					}
				}
			"
			FAILURE = "anb_goblin_quest.0005.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_owner = {
					add_prestige_no_experience = minor_prestige_loss
					increase_wounds_effect = { REASON = fight }
				}
				
				scope:inspiration = {
					change_inspiration_progress = -2
				}
			"
			SKILL = "prowess"
			DIFFICULTY = @medium_quest_duel
		}
	}
	
	# Negotiate
	option = {
		name = anb_goblin_quest.0005.b
		
		trigger = {
			NOT = {
				scope:inspiration_owner = {
					has_trait = diplomat
				}
			}
		}
		
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0005.a.t"
			SUCCESS = "anb_goblin_quest.0005.a.duel.success"
			SUCCESS_EFFECT = "
				scope:inspiration = {
					change_inspiration_progress = 2
					set_variable = settled_goblins
				}
				
				scope:quest_location = {
					change_development_progress_with_overflow = 25
					add_county_modifier = {
						modifier = accepted_goblin_settlers
						days = 1825
					}
				}
			"
			FAILURE = "anb_goblin_quest.0005.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_owner = {
					add_prestige_no_experience = minor_prestige_loss
					increase_wounds_effect = { REASON = fight }
				}
				
				scope:inspiration = {
					change_inspiration_progress = -2
				}
			"
			SKILL = "diplomacy"
			DIFFICULTY = @medium_quest_duel
		}
	}
	
	# Negotiate (diplomat)
	option = {
		name = anb_goblin_quest.0005.c
		trigger = {
			scope:inspiration_owner = {
				has_trait = diplomat
			}
		}
		
		trait = diplomat
		
		scope:inspiration = {
			change_inspiration_progress = 2
			set_variable = settled_goblins
		}
		
		scope:quest_location = {
			change_development_progress_with_overflow = 25
			add_county_modifier = {
				modifier = accepted_goblin_settlers
				days = 1825
			}
		}
	}
	
	# Sick the peasants on them
	option = {
		name = anb_goblin_quest.0005.d
		
		trigger = {
			scope:inspiration = {
				has_variable = recruited_peasants
			}
		}
		
		random_list = {
			# The goblins surrender
			25 = {
				scope:inspiration = {
					set_variable = defeated_goblin_boss
					change_inspiration_progress = 3
				}

				show_toast_for_owner_and_sponsor = {
					TITLE = anb_goblin_quest.0005.a.t
					ICON = scope:inspiration_owner
					TOOLTIP = anb_goblin_quest.0005.d.goblins_surrender
				}
			}
			# The goblins flee from their lair
			25 = {
				scope:inspiration = {
					set_variable = defeated_goblin_boss
					change_inspiration_progress = 2
				}
				
				show_toast_for_owner_and_sponsor = {
					TITLE = anb_goblin_quest.0005.a.t
					ICON = scope:inspiration_owner
					TOOLTIP = anb_goblin_quest.0005.d.goblins_flee
				}
			}
			# A new goblin boss arises
			25 = {
				scope:inspiration = {
					change_inspiration_progress = 1
				}
				
				scope:inspiration_owner = {
					add_prestige = medium_prestige_gain
				}
				
				show_toast_for_owner_and_sponsor = {
					TITLE = anb_goblin_quest.0005.a.t
					ICON = scope:inspiration_owner
					TOOLTIP = anb_goblin_quest.0005.d.new_goblin_boss
				}
			}
			# Goblins massacred
			25 = {
				modifier = {
					factor = 1.5
					scope:inspiration_owner = {
						has_trait = zealous
					}
				}
	
				scope:inspiration = {
					change_inspiration_progress = 5
				}
				
				show_toast_for_owner_and_sponsor = {
					TITLE = anb_goblin_quest.0005.a.t
					ICON = scope:inspiration_owner
					TOOLTIP = anb_goblin_quest.0005.d.goblins_massacred
				}
			}
		}
	}
	
	# Flee
	option = {
		name = anb_goblin_quest.0005.e
		
		scope:inspiration_owner = {
			add_prestige_no_experience = minor_prestige_loss
		}
		
		random = {
			chance = 33
			increase_wounds_effect = { REASON = attacked }
		}
	}
}

# Goblins fortiy lair
anb_goblin_quest.0006 = {
	title = anb_goblin_quest.0006.title
	desc = anb_goblin_quest.0006.description
	theme = war
	
	left_portrait = scope:inspiration_owner
	
	immediate = {
		scope:quest_location = {
			add_county_modifier = {
				modifier = fortified_goblin_lair
				days = 1825
			}
		}
	}
	
	trigger = {
		scope:inspiration = {
			OR = {
				inspiration_progress >= 4
				has_variable = found_goblin_lair
			}
		}
	}
	
	# Storm it with the peasants
	option = {
		name = anb_goblin_quest.0006.a
		trigger = {
			scope:inspiration = {
				has_variable = rallied_peasants
			}
		}
		highlight = yes
		
		scope:quest_location = {
			remove_county_modifier = fortified_goblin_lair
			add_county_modifier = {
				modifier = claimed_goblin_fortifications
				days = 1825
			}
		}
		
		scope:inspiration = {
			set_variable = claimed_goblin_fortifications
		}
	}
	
	# Sneak in (intrigue)
	option = {
		name = anb_goblin_quest.0006.b
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0005.a.t"
			SUCCESS = "anb_goblin_quest.0005.a.duel.success"
			SUCCESS_EFFECT = "
				scope:quest_location = {
					remove_county_modifier = fortified_goblin_lair
					add_county_modifier = {
						name = claimed_goblin_fortifications
						duration = 1825
					}
				}
				
				scope:inspiration = {
					set_variable = claimed_goblin_fortifications
				}
			"
			FAILURE = "anb_goblin_quest.0005.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_owner = {
					add_prestige_no_experience = minor_prestige_loss
					increase_wounds_effect = { REASON = attacked }
				}
				
				scope:inspiration = {
					change_inspiration_progress = -2
				}
			"
			SKILL = "intrigue"
			DIFFICULTY = @medium_quest_duel
		}
	}
	
	# We will bide our time then
	option = {
		name = anb_goblin_quest.0006.c
		scope:inspiration = {
			change_inspiration_progress = -1
		}
	}
}

# Rally peasants to fight goblins
anb_goblin_quest.0007 = {
	title = anb_goblin_quest.0007.title
	desc = anb_goblin_quest.0007.description
	theme = war
	
	left_portrait = scope:inspiration_owner

	# Rally (martial)
	option = {
		name = anb_goblin_quest.0007.a
		
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0007.a.t"
			SUCCESS = "anb_goblin_quest.0007.a.duel.success"
			SUCCESS_EFFECT = "goblin_rally_peasants_success_effect = yes"
			FAILURE = "anb_goblin_quest.0007.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_sponsor = {
					add_prestige_no_experience = minor_prestige_loss
				}
			"
			SKILL = "martial"
			DIFFICULTY = @easy_quest_duel
		}
	}
	
	# Rally (learning)
	option = {
		name = anb_goblin_quest.0007.b
		
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0007.a.t"
			SUCCESS = "anb_goblin_quest.0007.a.duel.success"
			SUCCESS_EFFECT = "goblin_rally_peasants_success_effect = yes"
			FAILURE = "anb_goblin_quest.0007.b.duel.failure"
			FAILURE_EFFECT = "
				scope:inspiration_sponsor = {
					add_prestige_no_experience = minor_prestige_loss
				}
			"
			SKILL = "learning"
			DIFFICULTY = @easy_quest_duel
		}
	}
	
	# No
	option = {
		name = anb_goblin_quest.0007.c
	}
}

# Goblins request to settle
anb_goblin_quest.0008 = {
	title = anb_goblin_quest.0008.title
	desc = anb_goblin_quest.0008.description
	theme = corruption
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			add = 1
			scope:inspiration = {
				has_variable = freed_goblin_slaves
			}
		}
		modifier = {
			factor = 0.85
			scope:inspiration = {
				has_variable = settled_goblins
			}
		}
		modifier = {
			factor = 0.1
			scope:inspiration = {
				has_variable = goblins_massacred
			}
		}
	}
	
	trigger = {
		NOT = {
			scope:location = {
				has_county_modifier = goblin_slave_labour
			}
		}
	}
	
	# Of course
	option = {
		name = anb_goblin_quest.0008.a
		
		scope:quest_location = {
			add_county_modifier = {
				modifier = accepted_goblin_settlers
				days = 1825
			}
		}
		
		random = {
			chance = 75
			
			scope:inspiration = {
				change_inspiration_progress = 1
			}
		}
	}
	
	# No
	option = {
		name = anb_goblin_quest.0008.a
	}
}

# Peasants use goblins as slaves
anb_goblin_quest.0009 = {
	title = anb_goblin_quest.0009.title
	desc = anb_goblin_quest.0009.description
	theme = corruption
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			add = 2
			scope:inspiration = {
				has_variable = defeated_goblin_band
			}
		}
		modifier = {
			add = 2
			scope:inspiration = {
				has_variable = settled_goblins
			}
		}
		modifier = {
			factor = 0.35
			scope:inspiration_owner = {
				has_character_modifier = freed_goblin_slaves
			}
		}
	}
	
	# Leave them be
	option = {
		name = anb_goblin_quest.0009.a
		
		scope:quest_location = {
			add_county_modifier = {
				modifier = goblin_slave_labour
				days = 1825
			}
		}
	}
	
	# Convince the peasants to free them
	option = {
		name = anb_goblin_quest.0009.b
		trigger = {
			scope:inspiration_owner = {
				NOT = { has_trait = diplomat }
			}
		}
		
		quest_duel_effect = {
			TITLE = "anb_goblin_quest.0009.a.t"
			SUCCESS = "anb_goblin_quest.0009.a.duel.success"
			SUCCESS_EFFECT = "
				scope:inspiration_owner = {
					add_character_modifier = {
						modifier = freed_goblin_slaves
						days = 1825
					}
					
					add_piety = minor_piety_gain
				}
				
				scope:inspiration_sponsor = {
					add_character_modifier = {
						modifier = freed_goblin_slaves
						days = 1825
					}
					
					add_piety = minor_piety_gain
				}
				
				scope:inspiration = {
					set_variable = freed_goblin_slaves
				}
			"
			FAILURE = "anb_goblin_quest.0009.b.duel.failure"
			FAILURE_EFFECT = "
				scope:quest_location = {
					add_county_modifier = {
						modifier = goblin_slave_labour
						days = 1825
					}
				}
				
				scope:inspiration_sponsor = {
					add_piety_no_experience = minor_piety_loss
				}
			"
			SKILL = "diplomacy"
			DIFFICULTY = @easy_quest_duel
		}
	}
	
	# Convince the peasants to free them (diplomat)
	option = {
		name = anb_goblin_quest.0009.b
		trigger = {
			scope:inspiration_owner = {
				has_trait = diplomat
			}
		}
		
		trait = diplomat
		
		scope:inspiration_owner = {
			add_character_modifier = {
				modifier = freed_goblin_slaves
				days = 1825
			}
			
			add_piety = minor_piety_gain
		}
		
		scope:inspiration_sponsor = {
			add_character_modifier = {
				modifier = freed_goblin_slaves
				days = 1825
			}
			
			add_piety = minor_piety_gain
		}
		
		scope:inspiration = {
			set_variable = freed_goblin_slaves
		}
	}
}

# Goblin treasure found
anb_goblin_quest.0010 = {
	title = anb_goblin_quest.0010.title
	desc = anb_goblin_quest.0010.description
	theme = stewardship
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			add = 1
			scope:inspiration = {
				has_variable = defeated_goblin_band
			}
		}
		modifier = {
			add = 1
			scope:inspiration = {
				has_variable = claimed_goblin_fortifications
			}
		}
		modifier = {
			add = 3
			scope:inspiration = {
				has_variable = defeated_goblin_boss
			}
		}
	}
	
	# Give it all to the adventurer
	option = {
		name = anb_goblin_quest.0010.a
		
		scope:inspiration_owner = {
			add_gold = 200
			
			if = {
				limit = {
					scope:inspiration_owner = {
						has_trait = greedy
					}
				}
				scope:inspiration_owner = {			
					add_opinion = {
						modifier = respect_opinion
						opinion = 25
						target = scope:inspiration_sponsor
					}
				}
			}
			else = {
				scope:inspiration_owner = {			
					add_opinion = {
						modifier = respect_opinion
						opinion = 15
						target = scope:inspiration_sponsor
					}
				}
			}
		}
		
		add_piety = minor_piety_gain
	}
	
	# Split it with the adventurer
	option = {
		name = anb_goblin_quest.0010.b
		
		scope:inspiration_owner = {
			add_gold = 100
		}
		
		add_gold = 100
	}
	
	# Return it to the locals
	option = {
		name = anb_goblin_quest.0010.c
		if = {
			limit = {
				scope:inspiration_owner = {
					has_trait = greedy
				}
			}
			scope:inspiration_owner = {			
				add_opinion = {
					modifier = respect_opinion
					opinion = -10
					target = scope:inspiration_sponsor
				}
			}
		}
		
		scope:quest_location = {
			add_county_modifier = {
				modifier = returned_goblin_loot
				days = 1825
			}
		}
	}
}

# Local garrison slaughters goblins
anb_goblin_quest.0011 = {
	title = anb_goblin_quest.0011.title
	desc = anb_goblin_quest.0011.description
	theme = war
	
	left_portrait = scope:inspiration_owner
	
	weight_multiplier = {
		base = 1
		modifier = {
			add = -0.2
			scope:quest_location = {
				has_county_modifier = fortified_goblin_lair
			}
		}
		modifier = {
			add = -0.4
			NOT = {
				scope:inspiration = {
					inspiration_progress >= 6
				}
			}
		}
	}
	
	trigger = {
		quest_location = {
			has_county_modifier = army_quest_presence
		}
	}
	
	# I see
	option = {
		name = anb_goblin_quest.0011.a
		
		scope:quest_location = {
			remove_county_modifier = fortified_goblin_lair
			remove_county_modifier = accepted_goblin_settlers
		}
		
		scope:inspiration = {
			remove_variable = settled_goblins
			set_variable = goblins_massacred
			
			random = {
				chance = 10
				change_inspiration_progress = 1
			}
		}
	}
}