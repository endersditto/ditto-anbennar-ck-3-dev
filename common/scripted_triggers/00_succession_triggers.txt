﻿#Special succession types
#If adding new types to the decision, use a trigger, otherwise they may break if the culture is split.

special_succession_witengamot_trigger = {
	culture = { has_cultural_tradition = tradition_the_witenagemot }
}

special_succession_thing_trigger = {
	culture = { has_cultural_pillar = heritage_north_germanic }
}

special_succession_tanistry_trigger = {
	culture = { has_cultural_pillar = heritage_goidelic }
}

historical_succession_access_single_heir_succession_law_trigger = {
	OR = {
		#Anbennar changed these to our own
		has_title = title:e_bulwar
		has_title = title:k_kheterata
		#Add any subsequent exceptions here.
	}
}

historical_succession_access_single_heir_succession_law_youngest_trigger = {
	OR = {
		#Anbennar changed these to our own
		has_title = title:e_bulwar
		has_title = title:k_kheterata
		#Add any subsequent exceptions here.
	}
}

historical_succession_access_single_heir_dynasty_house_trigger = {
	# AND = {
		# has_title = title:d_bohemia
		# culture = { has_innovation = innovation_table_of_princes }
	# }
	# Anbennar
	always = no
}