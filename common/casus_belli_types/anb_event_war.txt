﻿reachmman_independence_war = {
	group = civil_war
	ai_only_against_liege = yes
	target_titles = independence_domain
	
	allowed_for_character = {}

	allowed_against_character = {
		scope:attacker = {
			liege = scope:defender
		}
	}

	target_de_jure_regions_above = yes

	valid_to_start = {

	}

	on_victory_desc = {
		desc = independence_war_victory_desc
	}

	on_victory = {
		scope:attacker = { show_pow_release_message_effect = yes }
		create_title_and_vassal_change = {
			type = independency
			save_scope_as = change
		}
		# Grant all rebelling vassals indepence
		hidden_effect = {
			war = {
				every_war_attacker = {
					limit = {
						exists = liege
						liege = scope:defender
					}
					change_liege_or_become_independent = {
						CHANGE = scope:change
						VASSAL = this
					}
					
					add_prestige = minor_prestige_value

					hidden_effect = {
						set_variable = {
							name = independence_war_former_liege
							value = scope:defender
						}
						add_truce_both_ways = {
							character = scope:defender
							days = 1825
							war = root.war
							result = victory
						}
					}
				}
			}
		}
		# Destroy the Kingdom of Adshaw, remove prestige and change crown authority
		scope:defender = {
			add_prestige = {
				value = medium_prestige_value
				multiply = -1
			}
			if = {
				limit = { has_title = title:k_adshaw }
				destroy_title = title:k_adshaw
				add_stress = 100
			}
			if = {
				limit = { has_realm_law = crown_authority_1 }
				add_realm_law = crown_authority_0
			}
			if = {
				limit = { has_realm_law = crown_authority_2 }
				add_realm_law = crown_authority_1
			}
			if = {
				limit = { has_realm_law = crown_authority_3 }
				add_realm_law = crown_authority_2
			}
		}
		resolve_title_and_vassal_change = scope:change
	}

	on_white_peace_desc = {
		first_valid = {
			triggered_desc = {
				trigger = { scope:defender = { is_local_player = yes } }
				desc = independence_war_white_peace_defender_desc
			}
			triggered_desc = {
				trigger = {
					scope:attacker.joined_faction = {
						any_faction_member = { is_local_player = yes }
					}
				}
				desc = independence_war_white_peace_attacker_desc
			}
			desc = independence_war_white_peace_desc
		}
		desc = independence_war_white_peace_end_desc
		
	}

	on_white_peace = {
		scope:attacker = { 
			show_pow_release_message_effect = yes 
			stress_impact = {
 				ambitious = medium_stress_impact_gain
 				arrogant = medium_stress_impact_gain
 			}
		}
		hidden_effect = {
			scope:attacker = {
				add_truce_both_ways = {
					character = scope:defender
					days = 1825
					war = root.war
					result = white_peace
				}
				joined_faction = {
					add_faction_discontent = -200
				}
			}
		}

		scope:defender = {
			stress_impact = {
 				arrogant = medium_stress_impact_gain
 			}

			add_character_flag = {
				flag = recent_independence_faction_war
				years = faction_war_white_peace_cooldown
			}
			add_prestige = minor_prestige_value
		}

		on_white_peace_faction_revolt_war = yes
	}

	on_defeat_desc = {
		first_valid = {
			triggered_desc = {
				trigger = { scope:defender = { is_local_player = yes } }
				desc = independence_defeat_defender_desc
			}
			triggered_desc = {
				trigger = {
					scope:attacker = {
						is_local_player = yes
					}
				}
				desc = independence_defeat_attacker_desc
			}
			desc = player_independence_war_defeat_desc #same loc as for player
		}
		desc = independence_defeat_end_desc
	}

	on_defeat = {
		scope:attacker = { show_pow_release_message_effect = yes }
		scope:defender = {
			add_character_flag = {
				flag = recent_independence_faction_war
				years = faction_war_defeat_cooldown
			}
			add_dread = medium_dread_gain
			# Prestige for Defender
			add_prestige = medium_prestige_value
			if = {
				limit = {
					any_courtier = {
						has_strong_claim_on = title:c_frostwall
						is_male = yes
					}
				}
				random_courtier = {
					limit = {
						has_strong_claim_on = title:c_frostwall
						is_male = yes
					}
					add_opinion = {
						modifier = received_title_county
						target = scope:defender
					}
					save_scope_as = frostwall_legitimate
				}
				title:c_frostwall = {
					change_county_control = -40
					create_title_and_vassal_change = {
						type = revoked
						save_scope_as = change
						add_claim_on_loss = yes
					}
					# Setup and execute the changes to titles and vassals.
					change_title_holder_include_vassals = {
						holder = scope:frostwall_legitimate
						change = scope:change
					}
					resolve_title_and_vassal_change = scope:change
				}
				if = {
					limit = {
						scope:defender = {
							can_add_hook = {
								type = favor_hook
								target = scope:frostwall_legitimate
							}
						}
					}
					scope:defender = {
						add_hook = {
							target = scope:frostwall_legitimate
							type = favor_hook
						}
					}
				}
			}
		}
		# Imprison rebelling dudes
		hidden_effect = {
			scope:attacker = {
				hard_imprison_character_effect = {
					TARGET = this
					IMPRISONER = scope:defender
				}
				scope:defender = {
					add_opinion = {
						target = prev
						modifier = vassal_lost_faction_revolt_war
					}
				}
			}
			
			on_lost_faction_revolt_war = yes
		}
	}

	on_invalidated_desc = msg_invalidate_war_title

	check_defender_inheritance_validity = no

	on_primary_attacker_death = inherit
	on_primary_defender_death = inherit

	transfer_behavior = transfer

	attacker_allies_inherit = yes
	defender_allies_inherit = yes

	war_name = "INDEPENDENCE_WAR_NAME"

	interface_priority = 80

	use_de_jure_wargoal_only = yes

	attacker_wargoal_percentage = 1.0
	defender_wargoal_percentage = 0.0 # A single occupation will do
	defender_ticking_warscore_delay = { days = 0 } # No need for a delay here since the defender actually needs to occupy something rather than starting in control

	max_attacker_score_from_battles = 100
	max_defender_score_from_battles = 50

	max_ai_diplo_distance_to_title = 500
}

war_of_the_bears_cb = {
	group = civil_war
	ai_only_against_liege = yes
	target_titles = claim
	allowed_for_character =	{
	}

	allowed_against_character = {
		scope:attacker = {
			liege = scope:defender
		}
	}

	target_de_jure_regions_above = yes

	valid_to_start = {}

	on_victory_desc = {
		desc = claimant_faction_war_victory_desc
		
	}

	on_victory = {
		scope:attacker = { show_pow_release_message_effect = yes }
		create_title_and_vassal_change = {
			type = conquest_claim
			save_scope_as = change
			add_claim_on_loss = yes
		}
		# Create a claim CB which is executed to handle title/vassal changes, in addition to being used to calculate Prestige awards for war participants.
		setup_claim_cb = {
			titles = target_titles
			attacker = scope:attacker
			defender = scope:defender
			claimant = scope:claimant
			change = scope:change
		}

		resolve_title_and_vassal_change = scope:change

		if = {
			limit = {
				NOT = { scope:claimant = scope:attacker }
				scope:claimant.primary_title.tier < scope:attacker.primary_title.tier
				trigger_if = {
					limit = {
						exists = scope:claimant.liege
					}
					NOT = { scope:claimant.liege = scope:attacker }
				}
				trigger_else = {
					always = yes
				}
			}

			create_title_and_vassal_change = {
				type = conquest_claim
				save_scope_as = change_two
				add_claim_on_loss = yes
			}
			scope:claimant = {
				change_liege = {
					liege = scope:attacker
					change = scope:change_two
				}
			}
			resolve_title_and_vassal_change = scope:change_two
		}

		if = {
			limit = {
				NOT = { scope:claimant = scope:attacker }
				scope:attacker = {
					can_add_hook = {
					  	type = favor_hook
					  	target = scope:claimant
					}
				}
			}
			scope:attacker = {
				add_hook = {
		  			target = scope:claimant
		  			type = favor_hook
		 		}
			}
		}

		# Attacker gets Prestige Experience, Defender loses Prestige, all other participants gain Prestige based on their war contribution.
		modify_all_participants_fame_values = {
			WINNER = scope:attacker
			LOSER = scope:defender
			FAME_BASE = scope:cb_prestige_factor # Set by 'setup_claim_cb'
			IS_RELIGIOUS_WAR = no
			WINNER_FAME_SCALE = 10
			LOSER_FAME_SCALE = -10
			WINNER_ALLY_FAME_SCALE = 10
			LOSER_ALLY_FAME_SCALE = 10
		}

		# truce
		add_truce_attacker_victory_effect = yes

		# FP1: note the victory for future memorialisation via stele (if applicable).
		scope:attacker = { fp1_remember_recent_conquest_victory_effect = yes }
	}

	on_white_peace_desc = {
		desc = claimant_faction_war_white_peace_desc
		
	}

	on_white_peace = {
		scope:attacker = {
			show_pow_release_message_effect = yes
			stress_impact = {
 				ambitious = medium_stress_impact_gain
 				arrogant = medium_stress_impact_gain
 			}
		}

		scope:defender = {
			stress_impact = {
 				arrogant = medium_stress_impact_gain
 			}
		}
		on_white_peace_faction_revolt_war = yes

		hidden_effect = {
			scope:attacker = {
				add_truce_both_ways = {
					character = scope:defender
					days = 1825
					war = root.war
					result = white_peace
				}
				if = {
					limit = { exists = joined_faction }
					joined_faction = {
						destroy_faction = yes # Destroy the faction if it wasn't already destroyed automatically.
					}
				}
			}
		}
	}

	on_defeat_desc = {
		desc = claimant_faction_war_defeat_desc
		
	}

	on_defeat = {
		scope:attacker = { show_pow_release_message_effect = yes }
		on_lost_faction_revolt_war = yes

		scope:defender = {
			add_dread = medium_dread_gain
			add_achievement_flag_effect = { FLAG = achievement_know_your_place_flag }
		}
	}

	check_attacker_inheritance_validity = no

	on_primary_attacker_death = inherit_faction
	on_primary_defender_death = inherit

	transfer_behavior = transfer

	attacker_allies_inherit = no
	defender_allies_inherit = yes

	war_name = "CLAIMANT_WAR_NAME"

	interface_priority = 80

	use_de_jure_wargoal_only = yes

	attacker_wargoal_percentage = 0.8

	max_attacker_score_from_battles = 50
	max_defender_score_from_battles = 100

	max_ai_diplo_distance_to_title = 500
}