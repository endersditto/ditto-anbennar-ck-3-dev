k_corvuria = {
	1000.1.1 = { change_development_level = 8 }
	1008.6.1 = {	#End of Liberation of Busilar by Phoenix Empire
		liege = "e_bulwar"
		holder = sun_elvish0005 #Denarion Denarzuir
	}
}

d_bal_dostan = {
	1008.6.1 = {
		holder = sun_elvish0005 #Denarion Denarzuir
	}
}

c_arca_corvur = {
	1000.1.1 = { change_development_level = 10 }
}

c_silcorvur = {
	1000.1.1 = { change_development_level = 7 }
}

d_blackwoods = {
	1000.1.1 = { change_development_level = 7 }
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1008.6.1 = {
		liege = "k_corvuria"
	}
}

c_karns_hold = {
	1000.1.1 = { change_development_level = 6 }
}

d_ravenhill = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "k_corvuria"
	}
}

c_ravenhill = {
	1000.1.1 = { change_development_level = 9 }
}

c_gablaine = {
	1000.1.1 = { change_development_level = 12 }
}

d_tiferben = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1008.6.1 = {
		liege = "k_corvuria"
	}
}

c_aelaintaire = {
	1000.1.1 = { change_development_level = 9 }
}

c_rotwall = {
	1000.1.1 = { change_development_level = 7 }
}