k_blademarches = {
	1000.1.1 = { change_development_level = 8 }
	1020.12.13 = {
		holder = 34 #Clarimonde of Oldhave
		# succession_laws = {
			# blademarches_calindal_succession
		# }
	}
}

d_blademarch = {
	1000.1.1 = { change_development_level = 9 }
}

c_bladeskeep = {
	1000.1.1 = { change_development_level = 11 }
}

d_clovenwood = {
	1000.1.1 = { change_development_level = 7 }
}

d_beastgrave = {
	1000.1.1 = { change_development_level = 9 }
}

c_beastgrave = {
	1000.1.1 = { change_development_level = 7 }
}

c_banesfork = {
	1000.1.1 = { change_development_level = 10 }
}

c_medirleigh = {
	1000.1.1 = { change_development_level = 9 }
}

c_hendon = {
	1000.1.1 = { change_development_level = 9 }
}