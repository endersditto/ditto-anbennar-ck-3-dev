k_esmaria = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.31 = {
		holder = 69	#Laren the Lecher
	}
}

d_silverforge = {
	1000.1.1 = { change_development_level = 12 }
	952.1.7 = {
		holder = 68 #Dhaine Silverhammer
		government = feudal_government
	}
	1021.11.1 = {
		liege = k_esmaria
	}
}

c_silverforge = {
	952.1.7 = {
		holder = 68 #Dhaine Silverhammer
		government = feudal_government
	}
}

d_cann_esmar = {
	1000.1.1 = { change_development_level = 11 }
	995.7.3 = {
		holder = 714	#Drosten River-Reaver
		government = feudal_government
	}
}

c_esmaraine = {
	1000.1.1 = { change_development_level = 17 }
	995.7.3 = {
		holder = 69	#Laren the Ugly
	}
}

d_bennon = {
	1018.8.19 = {
		holder = 70 #Jaye Bennon
		government = feudal_government
	}
}

c_oldpassage = {
	995.7.3 = {
		holder = 714	#Drosten River-Reaver
	}
}

c_bennonhill = {
	1018.8.19 = {
		holder = 70 #Jaye Bennon
	}
}

c_bennon = {
	1000.1.1 = { change_development_level = 11 }
	1018.8.19 = {
		holder = 70 #Jaye Bennon
	}
}

c_giberd = {
	1000.1.1 = { change_development_level = 14 }
	1018.8.19 = {
		holder = 70 #Jaye Bennon
	}
}

c_havorton = {
	1000.1.1 = { change_development_level = 10 }
	1018.8.19 = {
		holder = 70 #Jaye Bennon
	}
}

d_estallen = {
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
		government = feudal_government
	}
}

c_estallen = {
	1000.1.1 = { change_development_level = 11 }
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
	}
}

c_tomansford = {
	1000.1.1 = { change_development_level = 10 }
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
	}
}

c_hatters_green = {
	1000.1.1 = { change_development_level = 10 }
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
	}
}

c_terr = {
	1007.1.6 = {
		holder = 725
	}
	1022.1.1 = {
		liege = d_estallen
	}
}

c_pinklady = {
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
	}
}

d_songbarges = {
	1000.1.1 = { change_development_level = 10 }
}

c_deamoine = {
	995.7.3 = {
		holder = 714	#Drosten River-Reaver
	}
}

c_notesbridge = {
	995.7.3 = {
		holder = 714	#Drosten River-Reaver
	}
}

c_seinathil = {
	1000.1.1 = { change_development_level = 11 }
	1005.6.10 = {
		holder = 722	#Alarian Singkeep
	}
}

d_ryalanar = {
	1010.12.14 = {
		holder = 72 #Eren Ryalan
		government = feudal_government
	}
}

c_ryalanar = {
	1000.1.1 = { change_development_level = 9 }
}

d_konwell = {
	1010.12.14 = {
		holder = 72 #Eren Ryalan
		government = feudal_government
	}
}

c_konwell = {
	1000.1.1 = { change_development_level = 11 }
}

d_leslinpar = {
	1000.1.1 = { change_development_level = 9 }
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
		government = feudal_government
	}
}

c_leslinpar = {
	1000.1.1 = { change_development_level = 11 }
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

c_yellowford = {
	1000.1.1 = { change_development_level = 10 }
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

c_varaine = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

d_hearthswood = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
		government = feudal_government
	}
}

c_tendergrove = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

c_hearthswood = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

c_elvelenn = {
	1000.1.1 = { change_development_level = 7 }
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
	}
}

d_asheniande = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
		government = feudal_government
	}
}

c_ashfields = {
	1000.1.1 = { change_development_level = 9 }
	1005.10.31 = {
		holder = 74 #Arlen Havoran
	}
}

c_teinmas = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
		government = feudal_government
	}
	1021.11.31 = {	# Seized by Ibenion
		holder = 40001 #Ibenion ta'Lunetain
		liege = k_ibevar
		government = feudal_government
	}
}

c_livergrave = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
	}
}

c_cestirbridge = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
	}
}

c_embergarden = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
	}
}