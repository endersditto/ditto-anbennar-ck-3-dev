﻿## these assets are then used in the genes (game/common/genes/..) at the bottom of the file. 



## kheteratan ##

## male Clothes ##
male_clothes_secular_kheteratan_01_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_01_common_entity }
}
male_clothes_secular_kheteratan_01_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_01_low_entity }
}
male_clothes_secular_kheteratan_01_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_01_high_entity }
}


male_clothes_secular_kheteratan_02_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_02_common_entity }
}
male_clothes_secular_kheteratan_02_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_02_low_entity }
}
male_clothes_secular_kheteratan_02_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_02_high_entity }
}


male_clothes_secular_kheteratan_03_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_secular_kheteratan_03_common_entity }
}

## Female Clothes ##

female_clothes_secular_kheteratan_01_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_01_common_entity }
}
female_clothes_secular_kheteratan_01_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_01_low_entity }
}
female_clothes_secular_kheteratan_01_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_01_high_entity }
}


female_clothes_secular_kheteratan_02_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_02_common_entity }
}


female_clothes_secular_kheteratan_03_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_03_common_entity }
}
female_clothes_secular_kheteratan_03_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_03_low_entity }
}
female_clothes_secular_kheteratan_03_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothes_secular_kheteratan_03_high_entity }
}

## EGYPTIAN ##

male_clothes_egyptian_usekh_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_usekh_common_entity }
}

male_clothes_egyptian_usekh_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_usekh_low_entity }
}

male_clothes_egyptian_usekh_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_usekh_high_entity }
}

male_clothes_egyptian_shirt_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_shirt_common_entity }
}

male_clothes_egyptian_shirt_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_shirt_low_entity }
}

male_clothes_egyptian_shirt_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothes_egyptian_shirt_high_entity }
}


##########################################################################


## kheteratan headgears cultural


# These assets are then used in the genes (game/common/genes/..) at the bottom of the file. 

	#set_tags = "no_hair"
	#set_tags = "no_beard"
	#set_tags = "crown"
	#set_tags = "cap"
	#set_tags = "hat"
	#set_tags = "hood_beard"
	#set_tags = "cap_02"
	#set_tags = "helmet"
	#set_tags = "headdress"
	#set_tags = "cap,cap_02"
	#set_tags = "aventail"
	
####male	
	
male_headgear_secular_kheteratan_common_01 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_common_01_entity 	 }		
}
male_headgear_secular_kheteratan_common_01_color2 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_common_01_color2_entity 	 }		
}
male_headgear_secular_kheteratan_common_01_color3 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_common_01_color3_entity 	 }		
}
male_headgear_secular_kheteratan_common_01_color4 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_common_01_color4_entity 	 }		
}




male_headgear_secular_kheteratan_war_common_01 = {
	set_tags = "no_hair"
	set_tags = "helmet"
	set_tags = "aventail"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_war_common_01_entity 	 }		
}

male_headgear_secular_kheteratan_war_common_02 = {
	set_tags = "no_hair"
	set_tags = "helmet"
	set_tags = "aventail"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_secular_kheteratan_war_common_02_entity 	 }		
}





###Female


female_headgear_kheteratan_common_01 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_headgear_kheteratan_common_01_entity 	 }		
}
female_headgear_kheteratan_common_01_color2 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_headgear_kheteratan_common_01_color2_entity 	 }		
}
female_headgear_kheteratan_common_01_color3 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_headgear_kheteratan_common_01_color3_entity 	 }		
}
female_headgear_kheteratan_common_01_color4 = {	
	set_tags = "cap"
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_headgear_kheteratan_common_01_color4_entity 	 }		
}


female_helmet_kheteratan_war_common_01 = {
	set_tags = "no_hair"
	set_tags = "helmet"
	set_tags = "aventail"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_helmet_kheteratan_war_common_01_entity 	 }		
}

female_helmet_kheteratan_war_common_02 = {
	set_tags = "cap"
	set_tags = "helmet"
	set_tags = "aventail"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = female_helmet_kheteratan_war_common_02_entity 	 }		
}

## Egyptian ##

male_headgear_egyptian_nemes_common = {	
	set_tags = "no_hair"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_egyptian_nemes_common_entity 	 }
}

male_headgear_egyptian_nemes_low = {	
	set_tags = "no_hair"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_egyptian_nemes_low_entity 	 }
}

male_headgear_egyptian_nemes_high = {	
	set_tags = "no_hair"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_egyptian_nemes_high_entity 	 }
}

male_headgear_egyptian_false_beard = {	
	set_tags = "no_beard"
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_egyptian_false_beard_entity 	 }
}

male_headgear_egyptian_uraeus = {	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_egyptian_uraeus_entity 	 }
}


## EGYPTIAN ##

## Pharaoh Clothes ##

male_legwear_egyptian_shendyt_common = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_legwear_egyptian_shendyt_common_entity }
}

male_legwear_egyptian_shendyt_low = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_legwear_egyptian_shendyt_low_entity }
}

male_legwear_egyptian_shendyt_high = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_legwear_egyptian_shendyt_high_entity }
}

## EGYPTIAN ##

egyptian_bracelets = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = egyptian_bracelets_entity }
}


## kheteratan War armor ##

## male Clothes ##

male_clothing_kheteratan_war_nobility_01 = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = male_clothing_kheteratan_war_nobility_01_entity }
}


female_clothing_kheteratan_war_01 = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothing_kheteratan_war_01_entity }
}
female_clothing_kheteratan_war_01v2 = {
	entity = { required_tags = ""		shared_pose_entity = torso		entity = female_clothing_kheteratan_war_01v2_entity }
}


